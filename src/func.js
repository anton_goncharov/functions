const getSum = (str1, str2) => {
  if (typeof str1 !== "string" || typeof str2 !== "string" ){
    return false
  }
  if (Number.isNaN(Number(str1)) ||  Number.isNaN(Number(str2))){
    return false
  }
  return String(+str1 + + str2)
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  const count = listOfPosts.reduce((acc,item) => {
    if (item.author === authorName){
      acc.post +=1
    }
    if (item.comments){
      item.comments.forEach(comment => {
        if (comment.author === authorName) {
          acc.comments += 1
        }
      });
      }
    return acc
  }, {post:0,comments:0})
  return `Post:${count.post},comments:${count.comments}`
};

const tickets=(people)=> {
    if(people[0] === 25){
      let clarckMoney = 25
      for (let i = 1; i < people.length; i++) {
        const hangover = people[i] - 25;
        if (clarckMoney < hangover){
          return "NO"
        }else{
          clarckMoney+=25
        }
      }
      return "YES"
    }
  return "NO"
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
